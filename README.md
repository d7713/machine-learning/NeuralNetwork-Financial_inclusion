# Financial Inclusion - East Africa (Neural Network)

### By: Andrew Wairegi

## Description
To classify whether an individual is financially included or
not using a model. This will be done using a neural network.
This means that they will be classified as having a bank account or not.
Allow East Africa to be able to identify those that will be financially 
included or not, using demographic data.

[Open notebook]

## Setup/Installation instructions
1. Create a folder on your computer
2. Set it up as an empty repository (git init)
3. Clone this repository (using git clone https://...)
4. Upload the notebook from that folder to google drive
5. Open it
6. Upload the data files from the folder to the google collab (upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Matplotlib - A Visualization package
5. Seaborn - A Visualization package
7. Scikit learn - A Modelling package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/NeuralNetwork-Financial_inclusion/-/blob/main/Financial_inclusion_east_africa_(neural_network).ipynb
